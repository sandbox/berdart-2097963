<?php

namespace Drupal\robotstxt\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $cofig;

  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(\Drupal::config('robotstxt.settings'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'robotstxt_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['robotstxt'] = array(
      '#type' => 'textarea',
      '#title' => t('Contents of robots.txt'),
      '#default_value' => _robotstxt_get_content(),
      '#cols' => 60,
      '#rows' => 20,
      '#wysiwyg' => FALSE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    parent::submitForm($form, $form_state);

    $this->config->set('robotstxt', $form_state['values']['robotstxt']);
    $this->config->save();
  }
}
