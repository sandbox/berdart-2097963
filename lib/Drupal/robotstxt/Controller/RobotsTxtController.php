<?php

namespace Drupal\robotstxt\Controller;


use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller class for RobotsTxt module
 */
class RobotsTxtController extends ControllerBase {

  /**
   * Callback for route robotstxt.robot
   * @return Response
   */
  public function robotstxt() {
    $content = array();
    $content[] = _robotstxt_get_content();

    // Hook other modules for adding additional lines.
    if ($additions = \Drupal::moduleHandler()->invokeAll('robotstxt')) {
      $content = array_merge($content, $additions);
    }

    // Trim any extra whitespace and filter out empty strings.
    $content = array_map('trim', $content);
    $content = array_filter($content);

    $response = new Response(implode("\n", $content));
    $response->headers->set('Content-type', 'text/plain');
    return $response;
  }
}
